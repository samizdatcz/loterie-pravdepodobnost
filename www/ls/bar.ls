return if window.location.hash == \#game
scale = d3.scale.linear!
  ..domain [1e6 0]
  ..range [100 0]

class Bar
  (@amount, @count, @ratio) ->
    @ratio ?= "1 : #{ig.utils.formatNumber 750000 / @count}"
  height: ->
    h = scale @count
    "#{Math.min h, 400}%"

bars =
  new Bar "Prohraje<br> 500 Kč" 7
  new Bar "Prohraje<br> 300 Kč" 600000 "4 : 5"
  new Bar "Nevyhraje<br> nic" 75000
  new Bar "Vyhraje<br> 200 Kč" 16750
  new Bar "Vyhraje<br> 500 Kč" 38000
  new Bar "Vyhraje<br>1 000 Kč" 16750
  new Bar "Vyhraje<br>1 500 Kč" 3000
  new Bar "Vyhraje<br>4 500 Kč" 344
  new Bar "Vyhraje<br>9 500 Kč" 98
  new Bar "Vyhraje<br>49 500 Kč" 37
  new Bar "Vyhraje<br>100 tis. Kč" 11
  new Bar "Vyhraje<br>500 tis. Kč" 2
  new Bar "Vyhraje<br>10 mil. Kč" 1


container = d3.select ig.containers.base
  ..classed \bar yes
vis = container.append \div
  ..attr \class "vis zoom-0"
vis.selectAll \div .data bars .enter!append \div
  ..attr \class \bar-container
  ..append \div
    ..attr \class \bar
    ..append \div
      ..attr \class \area
        ..style \height (.height!)
        ..append \div
          ..attr \class \label-top
          ..html (.ratio)
        ..append \div
          ..attr \class \label-bottom
          ..html (.amount)
areas = container.selectAll \div.area
zooms = [1e2 1e3 1e4 1e5 1e6]
currentZoom = 0
zoom = ->
  switch currentZoom
    | 0 =>
      button.html "Více příblížit"
      topIndicator.html "cca 1 m"
    | 1 =>
      button.html "Ještě více příblížit"
      topIndicator.html "cca 10 m"
    | 2 =>
      button.html "Ještě o něco více příblížit"
      topIndicator.html "cca 100 m"
    | 3 =>
      button.html "Oddálit"
      topIndicator.html "cca 1 km"
    | 4 =>
      button.html "Přiblížit"
      topIndicator.html ""
  currentZoom++
  currentZoom %= zooms.length
  vis.attr \class "vis zoom-#{currentZoom}"
  topIndicator.attr \class "top-indicator zoom-#{currentZoom}"
  zoom = zooms[currentZoom]
  scale.range [zoom, 0]
  areas.style \height (.height!)

button = container.append \button
  ..attr \class \zoom
  ..html \Přiblížit
  ..on \click zoom
topIndicator = container.append \div
  ..attr \class "top-indicator zoom-0"
container.append \div
  ..attr \class \division
  ..append \div
    ..attr \class \win
    ..html "Výhry ➔"
  ..append \div
    ..attr \class \lose
    ..html "<div>➔</div> Prohry"
