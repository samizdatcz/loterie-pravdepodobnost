return unless window.location.hash == \#game
container = d3.select ig.containers.base
  ..classed \game yes
vis = container.append \div
  ..attr \class \vis
values = vis.append \div
  ..attr \class \values
table = values.append \table
  ..append \thead
    ..append \tr
      ..append \th .html "Cena"
      ..append \th .html "Výhra"

tbody = table.append \tbody

introParagraph = vis.append \p
  ..html "Zahrajte si na gamblera zadarmo. Stiskněte tlačítko <button>Vsadit</button> a zkuste, kolikrát musíte vsadit, abyste něco<br>vyhráli – a kolik vás to bude stát."
vis.append \button
  ..html "Vsadit"
counter = 0
fun = Math.round Math.random!

if window.localStorage?loterie_uid
  uid = parseInt that, 10
else
  uid = Math.round Math.random! * 4294967295
  window.localStorage?loterie_uid = uid
sid = Math.round Math.random! * 4294967295

class Bet
  ->
    @cost = 500
    @ordering = counter++
    rand = Math.random! * 750000
    if fun and @ordering == 1
      rand = 3000
    @win = switch
    | rand <= 1 => 1e7
    | rand <= 3 => 500000
    | rand <= 14 => 100000
    | rand <= 51 => 50000
    | rand <= 149 => 10000
    | rand <= 493 => 5000
    | rand <= 3493 => 2000
    | rand <= 20243 => 1500
    | rand <= 58243 => 1000
    | rand <= 74993 => 700
    | rand <= 149993 => 500
    | rand <= 749993 => 200
    | otherwise => 0

bets = []
sumWins = 0
sumCost = 0
bet = ->
  bet = new Bet!
  sumWins += bet.win
  sumCost += bet.cost
  bets.unshift bet
  update!

lineHeight = 27
lines = 20
fn = ig.utils.formatNumber
update = ->
  tbody.selectAll \tr .data (bets.slice 0, 10), (.ordering)
    ..enter!append \tr
      ..append \td
        ..html -> "#{fn it.cost} Kč"
      ..append \td
        ..attr \data-win (.win)
        ..html -> "#{fn it.win} Kč"
    ..style \top -> "#{lineHeight * (counter - it.ordering)}px"
    ..exit!
      ..classed \exiting yes
      ..style \top -> "#{lineHeight * (counter - it.ordering)}px"
      ..transition!
        ..delay 300
        ..remove!
  return unless sumCost
  win = if sumWins
    ww = if sumWins > sumCost
      "vyhráli <span class='total total-win'>#{fn sumWins - sumCost}&nbsp;Kč</span>."
    else
      "prohráli <span class='total total-lose'>#{fn sumCost - sumWins}&nbsp;Kč</span>."
    "Vyhráli jste <span class='win'>#{fn sumWins}&nbsp;Kč</span>,<br>stálo vás to ale <span class='cost'>#{fn sumCost}&nbsp;Kč</span>.<br>Celkem jste tak #ww"
  else
    "Zatím jste ani jednou nevyhráli, zato jste zaplatili <span class='cost'>#{fn sumCost}&nbsp;Kč</span> za losy."
  introParagraph
    ..attr \class \status
    ..html "Vsadili jste si <span class='count'>#{fn bets.length}×</span>.<br>#win"


update!
container.selectAll \button
  ..on \click bet

lastCounter = 0
lastDataCounter = 0
setInterval do
  ->
    if lastDataCounter != counter
      lastDataCounter := counter
      dataset = "uid=#{uid}&sid=#{sid}&sumWins=#{sumWins}&sumCost=#{sumCost}&counter=#{counter}&fun=#{fun}"
      d3.xhr "/loterie-stats/"
        .header "Content-Type" "application/x-www-form-urlencoded"
        .post dataset, (err, response) ->
          console.log response.responseText
  1000

setInterval do
  ->
    if lastCounter != counter
      ga? \send \event \gamble \gamble counter - lastCounter
      lastCounter := counter
  10000
